export class Column {
    columnName: string;
    colspan?: number;


    constructor(columnName: string, colspan?: number) {
        this.columnName = columnName;   
        this.colspan = colspan;
    }
}