export class Rows {
    value: any;
    isClickable?: boolean;
    isPopOver?: boolean;
    rowspan?: number;
    colspan?: number;
    isDate?: boolean;
    formatDate?: string;
    isImageLink?: boolean;

    constructor(
        { value, isClickable, isPopOver, rowspan, colspan, isDate, formatDate, isImageLink }:
            { value: any, isClickable?: boolean, isPopOver?: boolean, rowspan?: number, colspan?: number, isDate?: boolean, formatDate?: string, isImageLink?: boolean }
        
    ) {
        this.value = value,
        this.isClickable = isClickable || false,
        this.isPopOver = isPopOver || false,
        this.rowspan = rowspan
        this.colspan = colspan
        this.isDate = isDate || false
        this.formatDate = formatDate
        this.isImageLink = isImageLink || false
    }
}