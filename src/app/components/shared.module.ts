import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarModule } from 'ng-sidebar-v2';
import { BrowserModule } from '@angular/platform-browser';
import { TableComponent } from './table/table.component';

@NgModule({
    imports: [
        SidebarModule.forRoot()
    ],
    exports: [],
    declarations: [SidebarComponent],
    providers: [],
})
export class SharedModule { }
