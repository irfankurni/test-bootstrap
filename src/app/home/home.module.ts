import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeRoutingModule } from './home-routing.module';
import { SidebarComponent } from '../components/sidebar/sidebar.component';
import { SharedModule } from '../components/shared.module';
import { SidebarModule } from 'ng-sidebar-v2';



@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SidebarModule.forRoot()
  ]
})
export class HomeModule { }
