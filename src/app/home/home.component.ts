import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  _opened: boolean = true;

  _toggleSidebar() {
    this._opened = !this._opened;
  }
}
